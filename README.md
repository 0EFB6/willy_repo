# willy_repo

Main repository for Willy OS GNU/Linux.

This repo contains my custom packages and some official AUR packages.

## Adding it to your Arch System

Append the following into your  `/etc/pacman.conf` file:

```
[willy_repo]
SigLevel = Optional DatabaseOptional
Server = https://gitlab.com/0EFB6/$repo/-/raw/main/$arch
```
